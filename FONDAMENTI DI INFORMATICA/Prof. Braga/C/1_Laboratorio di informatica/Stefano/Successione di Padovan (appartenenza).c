#include <stdio.h>

int main(){
	
	int i=78, num=0, val=0;
	int p[4] = {1, 1, 1, 2};
	printf("----Successione di Padovan----\n");
	printf("Fino a che numero vuoi che ti mostri i numeri della successione? ");
	scanf("%d", &num);

	for(; i>0; i--){
		if(i==78){
			if(p[0]==num){
				printf("%d e' un numero della successione di padovan", num);
				val=1;
				break;
			}
		}
		else if(i==77){
			if(p[1]==num){
				printf("%d e' un numero della successione di padovan", num);
				val=1;
				break;
			}
		}
		else if(i==76){
			if(p[2]==num){
				printf("%d e' un numero della successione di padovan", num);
				val=1;
				break;
			}
		}
		else if(i<75){
			p[3]=p[1]+p[0];
			if(p[3]==num){
				printf("%d e' un numero della successione di padovan", num);
				val=1;
				break;
			}
			p[0]=p[1];
			p[1]=p[2];
			p[2]=p[3];
		}
	}
	
	if(val==0){
		printf("%d non e' un numero della successione di padovan", num);
	}
		
	return 0;
}
