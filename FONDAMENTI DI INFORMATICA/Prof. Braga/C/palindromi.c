#include <stdio.h>
#include <string.h>

int palindromo (char * p) 
{
    
    for(int i = 0; i < strlen(p) / 2; i++)
	{
        if(p[i] != p[(strlen(p)-1) - i])
		{
                return 0;
        }
		
    }
    
    return 1;
}

int main(void) 
{
	
	char * par;
	
	printf("Insert string: ");
	scanf("%s",par);
	
	printf("Checking if palidrome...\n");
	
	if(palindromo(par))
	{
		
		printf("%s is palindrome\n",par);
		
	}
	
	else 
	{
		
		printf("%s is NOT palindrome\n",par);
		
	}
	
	
}