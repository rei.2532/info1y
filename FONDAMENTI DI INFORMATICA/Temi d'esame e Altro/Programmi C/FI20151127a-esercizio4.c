/*
Si supponga di dover rappresentare in C un archivio contenente informazioni sulle stelle. Per ogni stella, occore rappresentare: nome della stella
(massimo 30 caratteri), massa della stella (espressa come rapporto rispetto alla massa solare), distanza dal sistema solare (espresso in anni luce) ed il
numero di pianeti che appartengono al suo sistema (massimo 50); inoltre, per ciascun pianeta, occorre anche memorizzare il nome del pianeta (massimo 30 caratteri),
la massa del pianeta (espressa come rapporto rispetto alla massa terrestre) e il periodo di rivoluzione (espresso in giorni).

a)	Si dichiari un tipo di dato adatto a rappresentare tutte le informazioni necessarie a rappresentare una stella
b)	Si dichiari una variabile archivio in grado di contenere le informazioni di 1000 stelle
c)	Si scriva un frammento di codice C che individui, fra tutti  i pianeti abitabili  (cioè con massa compresa fra 0.5 e 10 e con periodo di rivoluzione minore di
1000), quello che si trova nel sistema più vicino al sistema solare; quindi stampi a video il nome di questo pianeta ed il nome della stella attorno a cui gravita.

Note: non è necessario inizializzare o leggere i dati della variabile archivio, ma si può ipotizzare che essa contenga già i dati relativi alle 1000 stelle su cui
bisogna effettuare la ricerca descritta al punto C. Se ci fosse più di un pianeta abitabile a distanza minima dal sistema solare, è sufficiente stampare i dati di
uno soltanto di questi pianeti (e della sua stella). Se non si trova alcun pianeta abitabile, occorre invece comunicarlo con un messaggio.

*/

#include <stdio.h>
#include <stdlib.h>

typedef struct {
    char nome[30];
    float massa;
    float rivoluzione;
} pianeta;

typedef struct {
    char nome[30];
    float massa;
    float distanza;
    pianeta pianeti[50];
    int nPianeti;
} stella;

int main() {

    stella archivio[1000];

    // .... inizializzazione archivio ...

    int pianetaVicino;
    int stellaPianeta;
    float distanza = 0.0;

    for(int i=0; i<1000, i++) { //per ogni stella nell'archivio
        for(int j=0; j<archivio[i].nPianeti; j++) { //per ogni pianeta della stella
            float massa = archivio[i].pianeti[j].massa;
            int rivoluzione = archivio[i].pianeti[j].rivoluzione;
            if(massa >= 0.5f && massa <= 10f && rivoluzione < 1000f && (distanza == 0.0f || archivio[i].distanza < distanza)) { //pianeta abitabile ed è il più vicino (per ora) al sistema solare
                    pianetaVicino = j;
                    stellaPianeta = i;
                    distanza = archivio[i].distanza;
            }

            j = archivio[i].nPianeti; //fermo il for loop dei pianeti perchè me ne basta uno per sistema solare

            }
        }

    if(distanza != 0.0f)
        printf("Il pianeta abitabile più vicino al sistema solare è il pianeta %s che gravita attorno alla stella %s\n", archivio[stellaPianeta].pianeti[pianetaVicino].nome, archivio[stellaPianeta].nome);
    else {
        printf("Non è stato trovato nessun pianeta abitabile\n");
    }

    return 0;
}
